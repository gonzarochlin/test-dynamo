# Test Dynamo


## Aclaraciones

> El código fue desarrollado y testeado en Windows.

> En caso de necesitar además para la auditoría, se podría agregar una validación y un registro de los directorios a los que no se puede acceder por falta de permisos.

## Installation and runnig

1. Make sure that you have installed Node.js
2. Enter to the project and install the dependencies running `npm i`
3. Create the table `files` on your database
```
CREATE TABLE `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `path` text NOT NULL,
  `type` enum('D','F') DEFAULT NULL,
  PRIMARY KEY (`id`)
)
```
4. Go to `index.js` and edit the `connection` with your data.
Should be something like this:
```
host: 'localhost',
user: 'root',
password: '',
database: 'dynamo'
```
5. Run `node index.js` as admin.

```
sudo node index.js
```

