const fs = require('fs');
const mysql = require('mysql2');

const connection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'dynamo'
});

function readDirectory(dir) {
    const files = fs.readdirSync(dir);
    files.forEach(file => {
        const fullPath = `${dir === '/' ? '' : dir}/${file}`;
        fs.stat(fullPath, (err, stats) => {
            if (err) console.error(`Error on fs.stat() [${fullPath}`, err);
            try {
                const type = stats.isDirectory() ? 'D' : (stats.isFile() ? 'F' : null);
                const sql = `INSERT INTO files (name, path, type) VALUES ("${file}", "${fullPath}", "${type}")`;
                connection.query(sql, (error, results) => {
                    if (error) {
                        console.error('Error on connection query', error);
                    } else {
                        console.log(`${fullPath} se ha insertado en la base de datos`);
                    }
                });
                if (stats.isDirectory()) {
                  readDirectory(fullPath);
                }
            } catch(error) {
                console.error('Error on stat', `${fullPath}`, error, stats);
            }
        });
    });
}

readDirectory('/');
